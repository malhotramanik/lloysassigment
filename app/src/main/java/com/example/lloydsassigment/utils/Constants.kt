package com.example.lloydsassigment.utils

object Constants {

    const val BASE_URL = "https://fakestoreapi.com/"

    const val ERROR_MSG_HTTP_EXCEPTION = "An unexpected error occurs"
    const val ERROR_MSG_IO_EXCEPTION = "Couldn't reach the server"
}
package com.example.lloydsassigment.di

import com.example.lloydsassigment.data.mapper.ProductMapper
import com.example.lloydsassigment.data.remote.ProductStoreApi
import com.example.lloydsassigment.data.repository.ProductRepositoryImpl
import com.example.lloydsassigment.domain.repository.ProductRepository
import com.example.lloydsassigment.utils.Constants
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    @Singleton
    @Provides
    fun providesProductStoreApi(): ProductStoreApi {
        return Retrofit.Builder()
            .baseUrl(Constants.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build().create(ProductStoreApi::class.java)
    }

    @Singleton
    @Provides
    fun provideProductRepository(
        productStoreApi: ProductStoreApi,
        productMapper: ProductMapper
    ): ProductRepository {
        return ProductRepositoryImpl(productStoreApi, productMapper)
    }
}
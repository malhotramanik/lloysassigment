package com.example.lloydsassigment.data.remote.dto

data class Rating(
    val count: Int,
    val rate: Double
)
package com.example.lloydsassigment.data.mapper

import com.example.lloydsassigment.data.remote.dto.ProductDetailDto
import com.example.lloydsassigment.data.remote.dto.ProductDto
import com.example.lloydsassigment.domain.models.Product
import com.example.lloydsassigment.domain.models.ProductDetail
import javax.inject.Inject


class ProductMapper @Inject constructor() {

    fun toProduct(productDto: ProductDto) = with(productDto) {
        Product(
            id = id,
            image = image,
            price = price.toString(),
            title = title
        )
    }

    fun toProductDetail(productDetailDto: ProductDetailDto) = with(productDetailDto) {
        ProductDetail(
            id = id,
            image = image,
            price = price.toString(),
            rating = rating.rate.toString(),
            title = title,
            category = category,
            description = description
        )
    }


}

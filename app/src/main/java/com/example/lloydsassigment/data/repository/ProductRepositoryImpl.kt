package com.example.lloydsassigment.data.repository

import com.example.lloydsassigment.data.mapper.ProductMapper
import com.example.lloydsassigment.data.remote.ProductStoreApi
import com.example.lloydsassigment.domain.models.Product
import com.example.lloydsassigment.domain.models.ProductDetail
import com.example.lloydsassigment.domain.repository.ProductRepository
import com.example.lloydsassigment.utils.Constants.ERROR_MSG_HTTP_EXCEPTION
import com.example.lloydsassigment.utils.Constants.ERROR_MSG_IO_EXCEPTION
import com.example.lloydsassigment.utils.Resource
import retrofit2.HttpException
import java.io.IOException

class ProductRepositoryImpl(
    private val productStoreApi: ProductStoreApi,
    private val productMapper: ProductMapper
) : ProductRepository {

    override suspend fun getAllProducts(): Resource<List<Product>> {
        return try {
            val products = productStoreApi.getAllProducts().map { productMapper.toProduct(it) }
            Resource.Success(products)
        } catch (e: HttpException) {
            Resource.Error(message = e.localizedMessage ?: ERROR_MSG_HTTP_EXCEPTION)
        } catch (e: IOException) {
            Resource.Error(message = ERROR_MSG_IO_EXCEPTION)
        }
    }

    override suspend fun getProduct(productId: Int): Resource<ProductDetail> {
        return try {
            val product =
                productStoreApi.getProduct(productId).let { productMapper.toProductDetail(it) }
            Resource.Success(product)
        } catch (e: HttpException) {
            Resource.Error(message = e.localizedMessage ?: ERROR_MSG_HTTP_EXCEPTION)
        } catch (e: IOException) {
            Resource.Error(message = ERROR_MSG_IO_EXCEPTION)
        }
    }

}
package com.example.lloydsassigment.data.remote

import com.example.lloydsassigment.data.remote.dto.ProductDetailDto
import com.example.lloydsassigment.data.remote.dto.ProductDto
import retrofit2.http.GET
import retrofit2.http.Path

interface ProductStoreApi {

    @GET(value = "products")
    suspend fun getAllProducts(): List<ProductDto>

    @GET(value = "products/{productId}")
    suspend fun getProduct(@Path("productId") productId: Int): ProductDetailDto
}
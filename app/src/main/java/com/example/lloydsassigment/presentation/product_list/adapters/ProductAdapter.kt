package com.example.lloydsassigment.presentation.product_list.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.lloydsassigment.databinding.ItemProductBinding
import com.example.lloydsassigment.domain.models.Product

class ProductAdapter :
    RecyclerView.Adapter<ProductAdapter.ProductViewHolder>() {

    private var products: List<Product> = emptyList()

    inner class ProductViewHolder(val binding: ItemProductBinding) :
        RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductViewHolder {
        val binding = ItemProductBinding.inflate(LayoutInflater.from(parent.context), parent, false)

        return ProductViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ProductViewHolder, position: Int) {
        val product = products[position]
        val binding = holder.binding
        holder.itemView.apply {
            Glide.with(this).load(product.image).into(binding.imageView)
            binding.tvTitle.text = product.title
            binding.tvPrice.text = "$${product.price}"
            setOnClickListener {
                onItemClickListener?.let {
                    it(product)
                }
            }
        }
    }

    override fun getItemCount() = products.size

    private var onItemClickListener: ((Product) -> Unit)? = null

    fun setItemClickListener(listener: (Product) -> Unit) {
        onItemClickListener = listener
    }

    fun updateProductList(newList: List<Product>) {

        val diffCallback = ProductListDiff(this.products, newList)
        val diffResult = DiffUtil.calculateDiff(diffCallback)
        diffResult.dispatchUpdatesTo(this)

        this.products = newList

    }
}
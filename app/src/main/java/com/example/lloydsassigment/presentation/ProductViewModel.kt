package com.example.lloydsassigment.presentation

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.lloydsassigment.domain.models.Product
import com.example.lloydsassigment.domain.models.ProductDetail
import com.example.lloydsassigment.domain.use_cases.get_all_products.GetAllProductsUseCase
import com.example.lloydsassigment.domain.use_cases.get_product.GetProductUseCase
import com.example.lloydsassigment.utils.Resource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import javax.inject.Inject

@HiltViewModel
class ProductViewModel @Inject constructor(
    private val getAllProductsUseCase: GetAllProductsUseCase,
    private val getProductUseCase: GetProductUseCase
) :
    ViewModel() {

    private val _stateFlow = MutableStateFlow<Resource<List<Product>>>(Resource.Loading())
    val pListStateFlow = _stateFlow.asStateFlow()

    private val _productStateFlow = MutableStateFlow<Resource<ProductDetail>>(Resource.Loading())
    val productStateFlow = _productStateFlow.asStateFlow()

    init {
        getAllProducts()
    }

    private fun getAllProducts() {
        getAllProductsUseCase().onEach { result ->
            _stateFlow.emit(result)
        }.launchIn(viewModelScope)

    }

    fun getProduct(productId: Int) {
        getProductUseCase(productId).onEach { result ->
            _productStateFlow.emit(result)
        }.launchIn(viewModelScope)
    }
}
package com.example.lloydsassigment.presentation.product_detail.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.navArgs
import com.bumptech.glide.Glide
import com.example.lloydsassigment.databinding.FragmentDetailBinding
import com.example.lloydsassigment.presentation.MainActivity
import com.example.lloydsassigment.presentation.ProductViewModel
import com.example.lloydsassigment.utils.Resource
import com.example.lloydsassigment.utils.hide
import com.example.lloydsassigment.utils.show
import kotlinx.coroutines.flow.collectLatest

class DetailFragment : Fragment() {

    private lateinit var activity: MainActivity

    private val viewModel: ProductViewModel by activityViewModels()

    private var _binding: FragmentDetailBinding? = null
    private val binding get() = _binding!!

    private val argument: DetailFragmentArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentDetailBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        activity = requireActivity() as MainActivity

        viewModel.getProduct(argument.selectedProductId)

        lifecycleScope.launchWhenStarted {
            viewModel.productStateFlow.collectLatest { result ->

                when (result) {
                    is Resource.Loading -> {
                        activity.binding.progressBar.show()
                    }

                    is Resource.Success -> {
                        val product = result.data
                        product?.let {
                            Glide.with(requireContext()).load(product.image).into(binding.imageView)
                            binding.tvTitle.text = product.title
                            binding.tvPrice.text = "Price: $${product.price}"
                            binding.tvRating.text = "Rating: ${product.rating}/5"
                            binding.tvDescription.text = "Description:\n${product.description}"
                            binding.tvCategory.text = product.category
                        }
                        activity.binding.progressBar.hide()
                    }
                    is Resource.Error -> {
                        Toast.makeText(
                            requireContext(),
                            result.message,
                            Toast.LENGTH_LONG
                        ).show()
                        activity.binding.progressBar.hide()
                    }
                }

            }
        }
    }
}
package com.example.lloydsassigment.presentation.product_list.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import com.example.lloydsassigment.databinding.FragmentListBinding
import com.example.lloydsassigment.presentation.MainActivity
import com.example.lloydsassigment.presentation.ProductViewModel
import com.example.lloydsassigment.presentation.product_list.adapters.ProductAdapter
import com.example.lloydsassigment.utils.Resource
import com.example.lloydsassigment.utils.hide
import com.example.lloydsassigment.utils.show
import kotlinx.coroutines.flow.collectLatest

class ListFragment : Fragment() {

    private val viewModel: ProductViewModel by activityViewModels()
    private lateinit var activity: MainActivity

    private lateinit var productAdapter: ProductAdapter

    private var _binding: FragmentListBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentListBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        activity = requireActivity() as MainActivity

        initialiseRecycleView()

        lifecycleScope.launchWhenStarted {
            viewModel.pListStateFlow.collectLatest { result ->
                when (result) {
                    is Resource.Error -> {
                        Toast.makeText(
                            requireContext(),
                            result.message,
                            Toast.LENGTH_LONG
                        ).show()

                        activity.binding.progressBar.hide()
                    }

                    is Resource.Loading -> {
                        activity.binding.progressBar.show()
                    }

                    is Resource.Success -> {
                        val listOfProducts = result.data
                        listOfProducts?.let { list ->
                            productAdapter.updateProductList(list)
                        }
                        activity.binding.progressBar.hide()
                    }
                }
            }
        }

    }

    private fun initialiseRecycleView() {
        productAdapter = ProductAdapter()
        binding.rvProduct.apply {
            adapter = productAdapter
            layoutManager = GridLayoutManager(requireContext(), 2)
        }

        productAdapter.setItemClickListener { product ->
            val selectedProductId: Int = product.id
            val action =
                ListFragmentDirections.actionToDetailFragment(selectedProductId = selectedProductId)
            findNavController().navigate(action)
        }

    }
}
package com.example.lloydsassigment

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class LloydsApplication : Application()
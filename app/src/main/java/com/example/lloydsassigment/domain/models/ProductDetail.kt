package com.example.lloydsassigment.domain.models

data class ProductDetail(
    val id: Int,
    val category: String,
    val description: String,
    val image: String,
    val price: String,
    val rating: String,
    val title: String
)
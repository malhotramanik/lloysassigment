package com.example.lloydsassigment.domain.use_cases.get_all_products

import com.example.lloydsassigment.domain.models.Product
import com.example.lloydsassigment.domain.repository.ProductRepository
import com.example.lloydsassigment.utils.Resource
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class GetAllProductsUseCase @Inject constructor(
    private val repository: ProductRepository
) {
    operator fun invoke(): Flow<Resource<List<Product>>> = flow {
        emit(repository.getAllProducts())
    }
}
package com.example.lloydsassigment.domain.repository

import com.example.lloydsassigment.domain.models.Product
import com.example.lloydsassigment.domain.models.ProductDetail
import com.example.lloydsassigment.utils.Resource

interface ProductRepository {

    suspend fun getAllProducts(): Resource<List<Product>>

    suspend fun getProduct(productId: Int): Resource<ProductDetail>

}
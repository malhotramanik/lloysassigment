package com.example.lloydsassigment.domain.use_cases.get_product

import com.example.lloydsassigment.domain.models.ProductDetail
import com.example.lloydsassigment.domain.repository.ProductRepository
import com.example.lloydsassigment.utils.Resource
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class GetProductUseCase @Inject constructor(
    private val repository: ProductRepository
) {
    operator fun invoke(productId: Int): Flow<Resource<ProductDetail>> = flow {
        emit(repository.getProduct(productId))
    }
}
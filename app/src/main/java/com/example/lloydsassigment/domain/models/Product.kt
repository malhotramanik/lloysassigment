package com.example.lloydsassigment.domain.models

data class Product(
    val id: Int,
    val image: String,
    val price: String,
    val title: String
)
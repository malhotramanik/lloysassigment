package com.example.lloydsassigment.presentation.product_list.viewmodels

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.lloydsassigment.MainCoroutineRule
import com.example.lloydsassigment.data.repository.DummyProductRepository
import com.example.lloydsassigment.domain.use_cases.get_all_products.GetAllProductsUseCase
import com.example.lloydsassigment.domain.use_cases.get_product.GetProductUseCase
import com.example.lloydsassigment.presentation.ProductViewModel
import com.example.lloydsassigment.utils.Resource
import com.google.common.truth.Truth.assertThat
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.Before
import org.junit.Rule
import org.junit.Test

@ExperimentalCoroutinesApi
class ProductViewModelTest {

    @get:Rule
    var mainCoroutineRule = MainCoroutineRule()

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    private lateinit var productViewModel: ProductViewModel

    private lateinit var dummyProductRepository: DummyProductRepository

    @Before
    fun setup() {
        dummyProductRepository = DummyProductRepository()

        productViewModel = ProductViewModel(
            GetAllProductsUseCase(dummyProductRepository),
            GetProductUseCase(dummyProductRepository)
        )
    }

    @ExperimentalCoroutinesApi
    @Test
    fun testProductListIsPresent() {
        when (val value = productViewModel.pListStateFlow.value) {
            is Resource.Success -> {
                assertThat(value.data?.size).isGreaterThan(0)
            }
            is Resource.Error -> {
                assertThat(value).isInstanceOf(Resource.Error::class.java)
            }
            is Resource.Loading -> {
                assertThat(value).isInstanceOf(Resource.Loading::class.java)
            }
        }

    }

    @ExperimentalCoroutinesApi
    @Test
    fun testProductValue() {
        val testProductId = 1
        productViewModel.getProduct(testProductId)
        when (val value = productViewModel.productStateFlow.value) {
            is Resource.Success -> {
                assertThat(value.data?.id).isEqualTo(testProductId)
            }
            is Resource.Error -> {
                assertThat(value).isInstanceOf(Resource.Error::class.java)
            }
            is Resource.Loading -> {
                assertThat(value).isInstanceOf(Resource.Loading::class.java)
            }
        }


    }
}
package com.example.lloydsassigment.data.repository

import com.example.lloydsassigment.data.mapper.ProductMapper
import com.example.lloydsassigment.data.remote.ProductStoreApi
import com.example.lloydsassigment.domain.models.Product
import com.example.lloydsassigment.domain.models.ProductDetail
import com.example.lloydsassigment.domain.repository.ProductRepository
import com.example.lloydsassigment.utils.Constants
import com.example.lloydsassigment.utils.Resource
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import okio.BufferedSource
import okio.buffer
import okio.source
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class DummyProductRepository : ProductRepository {

    private val server = MockWebServer()

    private var service = Retrofit.Builder()
        .baseUrl(server.url(Constants.BASE_URL))
        .addConverterFactory(GsonConverterFactory.create())
        .build()
        .create(ProductStoreApi::class.java)

    private val productRepositoryImpl = ProductRepositoryImpl(service, ProductMapper())


    private fun getMockResponse(fileName: String): MockResponse {
        val mockResponse = MockResponse()
        lateinit var source: BufferedSource
        javaClass.classLoader?.let {
            val inputStream = it.getResourceAsStream(fileName)
            source = inputStream.source().buffer()
        }
        return mockResponse.setBody(source.readString(Charsets.UTF_8))
    }

    override suspend fun getAllProducts(): Resource<List<Product>> {
        val mockResponse = getMockResponse("product_list_success_response.json")
        server.enqueue(mockResponse)
        return productRepositoryImpl.getAllProducts()
    }

    override suspend fun getProduct(productId: Int): Resource<ProductDetail> {
        val mockResponse = getMockResponse("product_success_response.json")
        server.enqueue(mockResponse)
        val product = productRepositoryImpl.getProduct(productId)

        return if (product.data?.id == productId) product
        else Resource.Error(Constants.ERROR_MSG_IO_EXCEPTION)
    }
}
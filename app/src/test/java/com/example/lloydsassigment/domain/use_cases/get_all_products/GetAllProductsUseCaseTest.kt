package com.example.lloydsassigment.domain.use_cases.get_all_products

import com.example.lloydsassigment.data.repository.DummyProductRepository
import com.example.lloydsassigment.utils.Resource
import com.google.common.truth.Truth.assertThat
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Test

class GetAllProductsUseCaseTest {

    lateinit var getAllProductsUseCase: GetAllProductsUseCase
    lateinit var dummyProductRepository: DummyProductRepository

    @Before
    fun setUp() {
        dummyProductRepository = DummyProductRepository()
        getAllProductsUseCase = GetAllProductsUseCase(dummyProductRepository)

    }

    @Test
    fun `Check Product list is not empty`() {
        runBlocking {
            getAllProductsUseCase.invoke().collectLatest { value ->
                assertThat(value).isInstanceOf(Resource.Success::class.java)
                assertThat(value.data).isNotNull()
                assertThat(value.data?.size).isGreaterThan(0)
            }
        }

    }
}
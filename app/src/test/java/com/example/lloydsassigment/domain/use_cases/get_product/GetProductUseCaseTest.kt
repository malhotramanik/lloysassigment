package com.example.lloydsassigment.domain.use_cases.get_product

import com.example.lloydsassigment.data.repository.DummyProductRepository
import com.example.lloydsassigment.utils.Resource
import com.google.common.truth.Truth.assertThat
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Test

class GetProductUseCaseTest {

    lateinit var getProductUseCase: GetProductUseCase
    lateinit var dummyProductRepository: DummyProductRepository

    @Before
    fun setUp() {
        dummyProductRepository = DummyProductRepository()
        getProductUseCase = GetProductUseCase(dummyProductRepository)
    }

    @Test
    fun `Check Product is found with id`() {
        runBlocking {
            val testProductId = 1
            getProductUseCase.invoke(testProductId).collectLatest { value ->
                assertThat(value).isInstanceOf(Resource.Success::class.java)
                assertThat(value.data).isNotNull()
                assertThat(value.data?.id).isEqualTo(testProductId)
            }
        }

    }

    @Test
    fun `Check Error if Product is not found with id`() {
        runBlocking {
            val testProductId = 40
            getProductUseCase.invoke(testProductId).collectLatest { value ->
                assertThat(value).isInstanceOf(Resource.Error::class.java)
            }
        }

    }
}